var acl = require('../js/acl');

var uri_good = 'http://www.google.com/index.html';
var uri_bad = 'htp://google/index';

var path_good = '/user/dir/anotherdir/file.txt';
var path_bad = '/user\\test//dir';
    
// Test the uri ===============================================================
if (acl.is_uri(uri_good) === true) 
    console.log(uri_good + ' is good');
else 
    console.log(uri_good + ' is poorly formed'); 
    
if (acl.is_uri(uri_bad) === false) 
    console.log(uri_bad + ' is bad');
else
    console.log(uri_bad + ' is well formed');
// ============================================================================

// Test the path, although it's probably not as important =====================
if (acl.is_path(path_good) === true) 
    console.log(path_good + ' is good');
else 
    console.log(path_good + ' is poorly formed'); 
    
if (acl.is_path(path_bad) === false) 
    console.log(path_bad + ' is bad');
else
    console.log(path_bad + ' is well formed');
// ============================================================================
