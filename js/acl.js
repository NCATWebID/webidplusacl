// Work with the n3 profiles and such.

var fs = require('fs');
var http = require('http');
var url = require('url');
var n3 = require('n3');
var wac = require('wac');
var env = process.env;
var root = env.PWD;

var acl = function () {};

/*
 * @Param webid The webid that represents the agent that requires access
 * @Param file_path The path to the file that needs to be accessed
 */
acl.prototype.init = function (webid, file_path) {

	var webid = webid;
	// var hostname = url.
	// var file = fs.readFileSync(file_path);
	var profiles = {};
	var options = {
  		hostname: webid,
  		port: 80,
  		method: 'GET',
  		headers: {
    		'Content-Type': 'text/n3; application/rdf+xml; text/html'
  		}
	};
/*var fileCallbackOptions = {'baseUrl' : file_path, 'filename' :   + '.acl'}; var accessControl = wac({'graphCallback':wac.fileGraphCallback(fileCallbackOptions)});

    accessControl.hasAccess('http://example.com/resource', 'GET',
                    'http://example.com/agent#me', null, callback);
    */
	get_profile_doc(options, webid);

};

// @param str The string to be checked if it is a well formed uri
// @returns true For a string being a uri
// @returns false For a string that is not a uri
acl.prototype.is_uri = function (str) {

    // we need to determine if the path is actually a path or if it
    // is a uri.
    // If it is a uri then return true otherwise return false.
    // a uri would contain several things to look for; http, https    
    
    // This pattern was developed by http://forums.devshed.com/author/kravvitz
    var pattern = RegExp('^(https?:\/\/)?'+ // protocol
        '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
        '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
        '(\:\d+)?(\/[-a-z\d%_.~+]*)*'); // port and path

        // This part is giving issues =========================================
        // '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
        // '(\#[-a-z\d_]*)?$','i'); // fragment locater
        // ====================================================================

    if (!pattern.test(str)) {
        console.log("invalid URL.");
        return false;
    } else {
        return true;
    }
};

// Check if a string is a path
acl.prototype.is_path = function (str) {

    var pattern = new RegExp('(\/[-a-z\d%_.~+]*)*' /*path*/); 

    if (!pattern.test(str)) {
        console.log('Invalid path');
        return false;
    } else {
        return true;
    }

};

// This will be used to get the profile docs
var deref_uri = function (uri) {

    var file; 
    http.get(uri, function (res) {
        res.on('data', function (chunk) {
            console.log('Response body data received: ' + chunk.toString());
        });

        res.on('error', function (err) {
            console.log('There was an error: ' + err);
        });
    });

    return file;

};

/*
 * @Param webid The webid to be dereferenced
 * @return profile The profile document webid dereferences to
 */
var get_profile_doc = function (options, webid) {

	var res_body;
	// Make sure the web id is well formed
	var req = http.request(options, function (res) {
		res.on('data', function (chunk) {
			console.log('BODY: ' + chunk);
		});

		console.log('Got response: ' + res.statusCode);
	});

	req.on('error', function (e) {
		console.log('Got error: ' + e.message);
	});

};

module.exports = new acl();
