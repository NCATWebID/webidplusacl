/*Profile gen quickly generates RDF profiles from JSON data*/

/* BASIC PROFILE ===================================
 *
 * A basic user profile:
 *  fname, lname
 *  email
 *  what group they belong to
 *  who the know
 *  refs to project they have access to
 *
 * A basic group profile:
 *  group name
 *  group admin (WebID)
 *  group page (just render rdf file for now)
 *  a listing of projects
 *  a listing of members
 */

var fs = require('fs'); // For writing RDF profile documents
var N3 = require('n3'); // N3 RDF library 
var parser = N3.Parser(); // Get the N3 parser

// Get the webid_generation module NOT IMPLEMENTED YET
//var webid_gen = require('webid_gen'); 

var profile_gen = function() {};

/* 
 * @param data The data object that must contain at the least a WebID (uri)
 */
profile_gen.prototype.create_profile = function (data) {

    var foaf_ns = '@prefix foaf: <http://www.xmnls.com/foaf/spec/#>.';
    var foaf_pre = 'foaf';
    var cert_ont = '@prefix cert: <http://www.w3.org/ns/auth/cert/#>.';
    var cert_pre = 'cert';

    // Need to utilize env vars so that a profiles directory can be created locally to the project root
    // var profiles_dir = '/home/boolean/workspace/WebIDExt/GrpAuthSvr/profiles/';
    
    if (data === {} || data === null) {
        console.log('error: data not initialized yet! \n');
        return
    }

    // Store the data properties
    var name = data.first + ' ' + data.last;
    var name_nospace = data.first.toLowerCase() + data.last.toLowerCase();
    var profile_name = data.first.toLowerCase() + data.last.toLowerCase();
    var public_key = data.pub_key;
    var profile_str = '';
    var group_name = data.group_name.toLowerCase();

    // We need to check to see if the profile already exists.
    // If it does indeed already exist we need to return false
    // and then respond to the client form letting them know it already
    // exists.
    if (fs.existsSync(profiles_dir + profile_name + '.n3')) {
        console.log('err the profile already exists!');
        return false; 
    }

    console.log('DATA ===============================================');
    for (key in data) {
        console.log(key + ' : ' + data[key]);
    }
    console.log('END DATA ===========================================');
    // Check if the user is a group or a regular user
    if (data.group === 'yes') {
        data.name = group_name;
        profile_path = profiles_dir + group_name + '.n3';
        profile_str = foaf_ns + '\n'
                    + cert_ont + '\n'
                    + '_:' + group_name + ' a ' + foaf_pre + ':Group ;' + '\n'
                    + foaf_pre + ':name ' + '"' + group_name + '"' + ' ;\n'
                    + foaf_pre + ':members [] ;\n'
                    + cert_pre + ':PublicKey ' + '"' + public_key + '"' + '.\n'
    } else {
        data.name = name_nospace;
        profile_path = profiles_dir + profile_name + '.n3';
        profile_str = foaf_ns + '\n'
                    + cert_ont + '\n'
                    + '_:' + name_nospace + ' a ' + foaf_pre + ':Person ;' + '\n'
                    + foaf_pre + ':name ' + '"' + name + '"' + ' ;\n'
                    + foaf_pre + ':knows [] ;\n'
                    + cert_pre + ':PublicKey ' + '"' + public_key + '"' + '.\n';
    }
    
    //console.log(profile_str);

    // Synchronously write the profile to disk
    fs.writeFileSync(profile_path, profile_str);

}

module.exports = profile_gen;
