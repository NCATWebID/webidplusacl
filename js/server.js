#!/usr/bin/env node

// We want to handle command line args as well as http reqs

var fs = require('fs');
var http = require('http');
// Process command line args
var program = require('commander');
// n3 parsing module
var n3 = require('n3');
// Profile generator
var profiler = require('./profile_gen');
var acl = require('./acl');

var PORT = 3000;
var HOST = 'localhost';

var cwd = process.cwd();
console.log(cwd);

// Handle command line args
program
    .version('0.0.1')
    .option('-s, --server', 'Enable the server')
    .option('-w, --webid <uri>', 'WebID of agent')  
    .option('-f, --file <path>', 'Path to a resource')
    .parse(process.argv);

var webid = program.webid;
var file_path = program.file;

console.log(webid);
console.log(file_path);

// This will be used to get the profile docs
var deref_uri = function (uri) {

    var file; 
    http.get(uri, function (res) {
        res.on('data', function (chunk) {
            console.log('Response body data received: ' + chunk.toString());
        });

        res.on('error', function (err) {
            console.log('There was an error: ' + err);
        });
    });

    return file;

};

if (program.webid) {
    acl(webid, null);
}

if (program.file) {

}

if (program.server) {
    // Create an http server on the localhost
    var server = http.createServer(function (req, res) {
        
        // Get the request headers
        var headers = req.headers;

        // Log the request headers.
        for (key in headers) {
            console.log(key + ' : ' + headers[key]);
        }

        // Switch on the request url
        switch (req.url) {
            case '/':
                res.write('<h1><a href="/users"> Users </a></h1>');
                res.write('<h1><a href="/resources"> Resources </a></h1>');
                res.end();

                break;
            case '/users':

                fs.readdir(cwd + '/users', function (err, users) {
                    if (err) throw err;

                    res.write('<h1> Users </h1>');
                    res.write('<ul>');

                    for (var i = 0; i < users.length; i++) {
                        res.write('<li> ' + users[i] + '</li>');
                    }
                        
                    res.write('</ul>');
                    res.end();
                });
                
                break;
            case '/resources':

                fs.readdir(cwd + '/resources', function (err, files) {
                    if (err) throw err;

                    res.write('<h1> Resources </h1>');
                    res.write('<ul>');

                    for (var i = 0; i < files.length; i++) {
                        res.write('<li> ' + files[i] + '</li>');
                    }
                    
                    res.write('</ul>');
                    res.end();
                });

                break;
        }

    }).listen(PORT, function () {
        console.log('The server is running at: %s:%s', HOST, PORT);
    });
}
